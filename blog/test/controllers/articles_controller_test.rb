require 'test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article = articles(:one)
      get articles_url, headers: { Authorization: ActionController::HttpAuthentication::Basic.encode_credentials('dhh', 'secret') }

  end


  test "should not create article" do
    assert_no_difference('Article.count') do
      post articles_url, params: { article: { title: 'Hello Rails', text: '' } }
    end
   
  end
 
  


end
