class RatingsController < ApplicationController

	def create
	    @article = Article.find(params[:article_id])
		@comment = @article.ratings.create(rating_params)
	    redirect_to controller: 'articles'
	end

	private
    def rating_params
      params.require(:rating).permit(:score)
    end
end
